import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, confusion_matrix, ConfusionMatrixDisplay
from sklearn.model_selection import train_test_split, StratifiedKFold
import statsmodels.api as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler, PolynomialFeatures

pd.set_option('display.max_columns', None)


DataFrame = pd.read_csv('C:\\Users\\PC\\Desktop\\PM2.5 Particles\\ShenyangPM20100101_20151231.csv')


def PullDBInfo():
    print(DataFrame.shape)
    print(DataFrame.head(3))
    print(DataFrame.columns)
    print(DataFrame.dtypes)
    print("\n")


def NaNCheck(DataFrame):
    print("There are Missing Values: ", DataFrame.isnull().values.any(), "\t")
    if DataFrame.isnull().values.any() : print("Number of Missing Values per Column in %: \n", DataFrame.isnull().sum()/DataFrame.shape[0] * 100)
    else : print("There is no Missing Values \n")


def KeepOneStation(DataFrame):
    NDataFrame = DataFrame.drop(columns=['PM_Taiyuanjie', 'PM_Xiaoheyan'])
    return NDataFrame


def DropUncredible(DataFrame):
    Mask = DataFrame.isnull()
    Missing = Mask.mean(axis=1)
    Selected = DataFrame[Missing>0.2]
    DataFrame = DataFrame.drop(Selected.index)
    return DataFrame


def MeanReplacement(DataFrame):
    DataFrame['PM_US Post'] = DataFrame['PM_US Post'].fillna(DataFrame['PM_US Post'].mean())
    DataFrame['DEWP'] = DataFrame['DEWP'].fillna(DataFrame['DEWP'].mean())
    DataFrame['HUMI'] = DataFrame['HUMI'].fillna(DataFrame['HUMI'].mean())
    DataFrame['PRES'] = DataFrame['PRES'].fillna(DataFrame['PRES'].mean())
    DataFrame['TEMP'] = DataFrame['TEMP'].fillna(DataFrame['TEMP'].mean())
    DataFrame['cbwd'] = DataFrame['cbwd'].fillna('cv')
    DataFrame['Iws'] = DataFrame['Iws'].fillna(DataFrame['Iws'].mean())
    DataFrame['precipitation'] = DataFrame['precipitation'].fillna(DataFrame['precipitation'].mean())
    DataFrame['Iprec'] = DataFrame['Iprec'].fillna(DataFrame['Iprec'].mean())
    return DataFrame


def DropPMOutlierValues(DataFrame):
    PMLogical = DataFrame['PM_US Post'].describe()
    PMMean = PMLogical['mean']
    PMStd = PMLogical['std']
    PMOutliers = DataFrame[(DataFrame['PM_US Post'] < (PMMean - 7 * PMStd)) | (DataFrame['PM_US Post'] > (PMMean + 7 * PMStd))]
    DataFrame = DataFrame.drop(PMOutliers.index)
    return DataFrame


def PMCorrelation(DataFrame):
    print(DataFrame['PM_US Post'].corr(DataFrame['year']))
    print(DataFrame['PM_US Post'].corr(DataFrame['season']))
    print(DataFrame['PM_US Post'].corr(DataFrame['month']))
    print(DataFrame['PM_US Post'].corr(DataFrame['day']))
    print(DataFrame['PM_US Post'].corr(DataFrame['hour']))
    print(DataFrame['PM_US Post'].corr(DataFrame['DEWP']))
    print(DataFrame['PM_US Post'].corr(DataFrame['TEMP']))
    print(DataFrame['PM_US Post'].corr(DataFrame['HUMI']))
    print(DataFrame['PM_US Post'].corr(DataFrame['PRES']))
    print(DataFrame['PM_US Post'].corr(DataFrame['Iws']))
    print(DataFrame['PM_US Post'].corr(DataFrame['precipitation']))
    print(DataFrame['PM_US Post'].corr(DataFrame['Iprec']))


def VisualizePMScatter(DataFrame, a, b):
    x = DataFrame[a].values
    y = DataFrame[b].values
    plt.scatter(x,y)
    plt.show()


def VisualizePMBar(DataFrame, x, y):
    plt.bar(DataFrame[x], DataFrame[y])
    plt.xlabel(x)
    plt.ylabel(y)
    plt.show()


def CorrHeatmap(DataFrame):
    DataFrame = DataFrame.drop(columns=['cbwd'])
    corr_mat = DataFrame.corr()
    sb.heatmap(corr_mat, annot=True)
    plt.show()


def Dependecy(DataFrame, x, y):
    plt.scatter(DataFrame[x], DataFrame[y])
    plt.title('Dependecy of {} on {}'.format(x,y))
    plt.xlabel(x)
    plt.ylabel(y)
    plt.show()


def CbwdRefactor(DataFrame):
    DataFrame["cbwd"] = DataFrame["cbwd"].replace(
        {
            "NE":"0",
            "SE": "1",
            "SW": "2",
            "NW": "3",
            "cv": "4"
        }
    )
    return DataFrame


def ModelEvaluation(y, ypred, n, d):
    mse = mean_squared_error(y, ypred)
    mae = mean_absolute_error(y, ypred)
    rmse = np.sqrt(mse)
    r2 = r2_score(y, ypred)
    r2_adj = 1 - (1-r2)*(n-1)/(n-d-1)

    print('Mean squared error: ', mse)
    print('Mean absolute error: ', mae)
    print('Root mean squared error: ', rmse)
    print('R2 score: ', r2)
    print('R2 adjusted score: ', r2_adj)

    res = pd.concat([pd.DataFrame(y.values), pd.DataFrame(ypred)], axis=1)
    res.columns = ['y', 'ypred']
    print(res.head(20))


def RemoveCategorical(DataFrame):
    DataFrame = DataFrame.drop(['No', 'year', 'month', 'day', 'hour', 'season'], axis=1)
    return DataFrame


def SplitData(DataFrame):
    x = DataFrame.drop(['PM_US Post'], axis = 1).copy()
    y = DataFrame['PM_US Post'].copy()
    xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size = 0.3, random_state = 42)
    xval, xtest, yval, ytest = train_test_split(xtest, ytest, test_size = 0.5, random_state = 42)
    return xtrain, ytrain, xtest, ytest


def LinRegression(xtrain, ytrain, xtest, ytest, regularization):
    if regularization == 'Ridge':
        Model = Ridge(alpha=5)
    elif regularization == 'Lasso':
        Model = Lasso(alpha=0.01)
    else:
        Model = LinearRegression(fit_intercept = True)
    Model.fit(xtrain, ytrain)
    ypred = Model.predict(xtest)
    ModelEvaluation(ytest, ypred, xtrain.shape[0], xtrain.shape[1])
    plt.figure(figsize=(10,5))
    plt.bar(range(len(Model.coef_)), Model.coef_)
    plt.show()
    print("Coefficients: ", Model.coef_)


def Selection(xtrain, ytrain):
    x = sm.add_constant(xtrain)
    Model = sm.OLS(ytrain, x.astype('float')).fit()
    print(Model.summary())
    x = sm.add_constant(xtrain.drop('TEMP', axis=1))
    Model = sm.OLS(ytrain, x.astype('float')).fit()
    print(Model.summary())
    x = sm.add_constant(xtrain.drop('precipitation', axis=1))
    Model = sm.OLS(ytrain, x.astype('float')).fit()
    print(Model.summary())


def Standardization(xtrain, DataFrame):
    x = DataFrame.drop(['PM_US Post'], axis=1).copy()
    y = DataFrame['PM_US Post'].copy()
    numeric_feats = [item for item in x.columns if 'cbwd' not in item]
    #print(numeric_feats)
    dummy_feats = [item for item in x.columns if 'cbwd' in item]
    #print(dummy_feats)
    scaler = StandardScaler()
    scaler.fit(xtrain[numeric_feats])
    xtrain_std = pd.DataFrame(scaler.transform(xtrain[numeric_feats]), columns=numeric_feats)
    xtest_std = pd.DataFrame(scaler.transform(xtest[numeric_feats]), columns = numeric_feats)
    xtrain_std = pd.concat([xtrain_std, xtrain[dummy_feats].reset_index(drop=True)], axis=1)
    xtest_std = pd.concat([xtest_std, xtest[dummy_feats].reset_index(drop=True)], axis=1)
    xtrain_std.head()
    return xtrain_std, xtest_std


def PolynomialF(xtrain_std, xtest_std, deg):
    polynomial = PolynomialFeatures(degree=deg, interaction_only=True, include_bias=False)
    x_inter_train = polynomial.fit_transform(xtrain_std)
    x_inter_test = polynomial.transform(xtest_std)
    #print(polynomial.get_feature_names_out())
    return x_inter_train, x_inter_test


def Labeling(DataFrame):
    if DataFrame['PM_US Post'] <= 55.4:
        return 'Safe'
    elif DataFrame['PM_US Post'] > 55.4 and DataFrame['PM_US Post'] <= 150.4:
        return 'Unsafe'
    else:
        return 'Dangerous'


def ClassifierEvaluation(ConfMatrix):
    Labels = ['Safe', 'Unsafe', 'Dangerous']


def AccuracyTest(ConfMatrix, Labels):
    Accuracy_i = []
    N = ConfMatrix.shape[0]
    for i in range(N):
        j = np.delete(np.array(range(N)), i)
        TP = ConfMatrix[i, i]
        F = 0
        F = (sum(ConfMatrix[i,j]) + sum(ConfMatrix[i,j]))
        TN = sum(sum(ConfMatrix) - F - TP)
        Accuracy_i.append((TP + TN)/sum(sum(ConfMatrix)))
        print('For ', Labels[i], ' precision is: ', Accuracy_i[i])
    Accuracy_Avg = np.mean(Accuracy_i)
    return Accuracy_Avg


def SensitivityTest(ConfMatrix, Labels):
    Sensitivity_i = []
    N = ConfMatrix.shape[0]
    for i in range(N):
        j = np.delete(np.array(range(N)), i)
        TP = ConfMatrix[i, i]
        F = 0
        F = (sum(ConfMatrix[i,j]) + sum(ConfMatrix[i,j]))
        TN = sum(sum(ConfMatrix) - F - TP)
        Sensitivity_i.append((TP + TN)/sum(sum(ConfMatrix)))
        print('For ', Labels[i], ' precision is: ', Sensitivity_i[i])
    Sensitivity_Avg = np.mean(Sensitivity_i)
    return Sensitivity_Avg


def Stratified(x, y):
    KF = StratifiedKFold(n_splits=10, shuffle=True, random_state=42)
    indexes = KF.split(x, y)
    Labels = ['Safe', 'Unsafe', 'Dangerous']
    FinalConfMatrix = np.zeros((len(np.unique(y)), len(np.unique(y))))
    for train_index, test_index in indexes:
        xtrain = x.iloc[train_index, :]
        xtest = x.iloc[test_index, :]
        ytrain = y.iloc[train_index]
        ytest = y.iloc[test_index]
        Classifier = KNeighborsClassifier(n_neighbors=1, metric='euclidean')
        Classifier.fit(xtrain, ytrain)
        ypred = Classifier.predict(xtest)
        ConfMatrix = confusion_matrix(ytest, ypred, labels=Classifier.classes_)
        FinalConfMatrix = FinalConfMatrix + ConfMatrix
    print('Final Confusion Matrix: ')
    print(FinalConfMatrix)
    Accuracy = AccuracyTest(FinalConfMatrix, Labels)
    Sensitivity = SensitivityTest(FinalConfMatrix, Labels)
    print('Average accuracy rate: ', Accuracy)
    print('Average sensitivity rate: ', Sensitivity)

def KNN(x, y):
    xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.15, random_state=42, stratify=y)
    Classifier = KNeighborsClassifier(n_neighbors=1, metric='euclidean')
    Classifier.fit(xtrain, ytrain)
    ypred = Classifier.predict(xtest)
    ConfMatrix = confusion_matrix(ytest, ypred)
    ClassifierEvaluation(ConfMatrix)
    disp = ConfusionMatrixDisplay.from_predictions(y_true= ytest,  y_pred=ypred,  labels = Classifier.classes_, cmap=plt.cm.Blues)
    plt.show()


#PullDBInfo()
#print(DataFrame.describe())
#NaNCheck(DataFrame)
DataFrame = KeepOneStation(DataFrame)
DataFrame = DropUncredible(DataFrame)
DataFrame = MeanReplacement(DataFrame)
DataFrame = DropPMOutlierValues(DataFrame)
#print(DataFrame.describe())
#PMCorrelation(DataFrame)
#CorrHeatmap(DataFrame)
#VisualizePMBar(DataFrame, 'year', 'PM_US Post')
#Dependecy(DataFrame, 'PM_US Post', 'TEMP')
DataFrame = CbwdRefactor(DataFrame)
DataFrame = RemoveCategorical(DataFrame)
xtrain, ytrain, xtest, ytest = SplitData(DataFrame)
#LinRegression(xtrain, ytrain, xtest, ytest, regularization)
#Selection(xtrain, ytrain)
xtrain_std, xtest_std = Standardization(xtrain, DataFrame)
#LinRegression(xtrain_std, ytrain, xtest_std, ytest, regularization)
xinter_train, xinter_test = PolynomialF(xtrain_std, xtest_std, 2)
#LinRegression(xinter_train, ytrain, xinter_test, ytest, regularization)
regularization = 'Lasso'
#LinRegression(xinter_train, ytrain, xinter_test, ytest, regularization)
DataFrame['Label'] = DataFrame.apply(Labeling, axis=1)
x = DataFrame.iloc[:, :-1].copy()
y = DataFrame.iloc[:, -1].copy()
#Stratified(x, y)
#KNN(x, y)